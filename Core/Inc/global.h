/*******************************************************************************
  * @file           : global.h
  * @brief          : incluye todas las librerias y variables globales
  * @author			: Yerih Iturriago
  ******************************************************************************/

#include <sdram_os.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "main.h"
#include "usart.h"
#include "gpio.h"
#include "printf.h"
#include "fmc.h"
#include "cmsis_os.h"
#include "cmsis_os2.h"
#include "fatfs.h"
#include "sdmmc.h"
//#include "threads.h"
#include "fatfs.h"
#include "ff.h"
//#include "bsp_driver_sd.h"
//#include "sd_diskio.h"
#include "ff_gen_drv.h"
#include "led.h"
//#include "start.h"
#include "sd_os.h"
#include "utils.h"
//#include "unitTest_stm32h743.h"
//#include "audio_config.h"
#include "audio.h"
//#include "pad_def.h"
//#include "pad.h"
//#include "adc.h"
//#include "adc_os.h"
//#include "button.h"
#include "sdram_os.h"


#ifndef GLOBAL_H_
#define GLOBAL_H_

#define FALSE 0
#define TRUE  1



/********************* variables app ****************************/

extern int g_showDebug;
extern int g_SDmntState;
//extern st_AUDIO_BUFFER audioBuffer;
//extern osMessageQueueId_t msgStringQ;
//extern osMessageQueueId_t msgNumberQ;
//extern osEventFlagsId_t adcCmpltHandle;
//extern osEventFlagsId_t mixEventHandle;



extern uint8_t retSDRAMDISK; /* Return value for SDRAMDISK */
extern char SDRAMDISKPath[4]; /* SDRAMDISK logical drive path */
extern FATFS SDRAMDISKFatFS; /* File system object for SDRAMDISK logical drive */
extern FIL SDRAMDISKFile; /* File object for SDRAMDISK */

#endif



