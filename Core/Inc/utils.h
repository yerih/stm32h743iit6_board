/*******************************************************************************
  * @file           : utils.h
  * @brief          : utilidades
  * @author			: Yerih Iturriago
  * @note			:
  *
  ******************************************************************************/

#ifndef UTILS_H_
#define UTILS_H_

#include "global.h"



/******************************************************************
function: 	utils_dresult
input	:	dLabel: referido al enum DRESULT.
output	:	string de datos
descr	:	retorna el string del error segun frLabel.
*******************************************************************/
char* utils_dresult(uint8_t dLabel, char* str);

/******************************************************************
function: 	utils_fresult
input	:	frLabel: referido al enum FRESULT.
output	:	string de datos
descr	:	retorna el string del error segun frLabel.
*******************************************************************/
char* utils_fresult(uint8_t frLabel, char* str);


/******************************************************************
input	:	hLabel: referido al enum HAL STATUS.
output	:	string de datos
descr	:	retorna el string del error segun frLabel.
*******************************************************************/
char* utils_hresult(uint8_t hLabel, char* str);


/**********************************************************************************
input	:	v: value of 4 bytes. dst: string destiny.
output	:	n/a
descr	:	convierte un numero hex, invertido y contenido en un entero, en string
**********************************************************************************/
void utils_32b_hexToString(uint32_t v, char dst[]);

#endif
