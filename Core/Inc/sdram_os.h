/**
 *@file    sdram_fmc_drv.h
 *@brief   使用 FMC 操作 SDRAM
 *@author  mculover
 *@date    2020-08-27
 *@note    此驱动测试 W9825G6KH SDRAM芯片通过
*/

#ifndef _SDRAM_FMC_DRV_H_
#define _SDRAM_FMC_DRV_H_

#include "global.h"

extern SDRAM_HandleTypeDef  hsdram1;



typedef struct SDRAM_OS_struct
{
		const uint32_t	     addrOrigin;
		SDRAM_HandleTypeDef* hsdram;
		uint32_t			 cursor;
		uint32_t			 addr;
}SDRAM_OS_typedef;
extern SDRAM_OS_typedef sdram_os;


#define EXT_RAM_ADDR  	((uint32_t)0xC0000000)
#define EXT_RAM_SECTION __attribute__((section(".external_ram")))
#define EXT_RAM_TOTAL_WORDS 	 1024*1024*8
#define EXT_RAM_TOTAL_HALF_WORDS 1024*1024*16
#define EXT_RAM_TOTAL_BYTES 	 1024*1024*32
#define EXT_RAM_1MB_8B			 1024*1024
#define EXT_RAM_1MB_16B			 1024*1024/2
#define EXT_RAM_1MB_32B			 1024*1024/4

#define SDRAM_MODEREG_BURST_LENGTH_1             ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_LENGTH_2             ((uint16_t)0x0001)
#define SDRAM_MODEREG_BURST_LENGTH_4             ((uint16_t)0x0002)
#define SDRAM_MODEREG_BURST_LENGTH_8             ((uint16_t)0x0004)
#define SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL      ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_TYPE_INTERLEAVED     ((uint16_t)0x0008)
#define SDRAM_MODEREG_CAS_LATENCY_2              ((uint16_t)0x0020)
#define SDRAM_MODEREG_CAS_LATENCY_3              ((uint16_t)0x0030)
#define SDRAM_MODEREG_OPERATING_MODE_STANDARD    ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_PROGRAMMED ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_SINGLE     ((uint16_t)0x0200)

void sdram_os_init(void);
int sdram_os_sendCommand(uint32_t CommandMode, uint32_t Bank, uint32_t RefreshNum, uint32_t RegVal);


#endif /* _SDRAM_FMC_DRV_H_ */
