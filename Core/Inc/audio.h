/*******************************************************************************
  * @file           : audio.h
  * @brief          : librerias de audio
  * @author			: Yerih Iturriago
  ******************************************************************************/

#ifndef AUDIO_H_
#define AUDIO_H_

#include "global.h"


typedef struct st_WAVE_Format
{
	uint32_t ChunkID; 		/* 0 */
	uint32_t FileSize; 		/* 4 */
	uint32_t FileFormat; 	/* 8 */
	uint32_t SubChunk1ID; 	/* 12 */
	uint32_t SubChunk1Size; /* 16 */
	uint16_t AudioFormat; 	/* 20 */
	uint16_t NbrChannels; 	/* 22 */
	uint32_t SampleRate; 	/* 24 */
	uint32_t ByteRate; 		/* 28 */
	uint16_t BlockAlign; 	/* 32 */
	uint16_t BitPerSample; 	/* 34 */
	uint32_t SubChunk2ID; 	/* 36 */
	uint32_t SubChunk2Size; /* 40 */

} WAVE_headerFile_typedef;

#define WAVE_HEADER_SIZE	44




/******************************************************************
input	:	header del archivo.
output	:	string de datos
descr	:	retorna el string del error segun frLabel.
*******************************************************************/
void audio_printWaveHeader(WAVE_headerFile_typedef* header);


/******************************************************************
input	:	header del archivo.
output	:	string de datos
descr	:	retorna el header en string. Recuerde hacer un pvPortFree().
*******************************************************************/
char* audio_waveHeaderToString(WAVE_headerFile_typedef* header);

void audio_readWavFileUntilEnd(FIL* SDFile, void* dst, uint32_t len);
void audio_cleanAudioBuffer_16b(int16_t* b, uint32_t len);

#endif

