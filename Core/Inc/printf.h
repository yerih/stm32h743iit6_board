/*******************************************************************************
  * @file           : printf.c
  * @brief          : control el printf.
  * @author			: Yerih Iturriago
  * @description	: configuracion del printf via serial.
  ******************************************************************************/


#ifndef PRINTF_H_
#define PRINTF_H_

#include <stdint.h>
#include <stdio.h>
#include "usart.h"



extern UART_HandleTypeDef huart4;
extern UART_HandleTypeDef huart5;





#ifdef __GNUC__
  /* With GCC, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
PUTCHAR_PROTOTYPE;




#endif

