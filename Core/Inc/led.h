/*******************************************************************************
  * @file           : led.c
  * @brief          : incluye todas las librerias y variables globales
  * @author			: Yerih Iturriago
  ******************************************************************************/

#ifndef LED_H_
#define LED_H_

#include "gpio.h"

/********************* LED configuracion ****************************
*	Colocar en LED_n_PIN el numero de pin correspondiente.
*	Ejemplo: LED_1_PIN	->  GPIO_PIN_1
*
*********************************************************************/
#define LED_0_PIN	  GPIO_PIN_0
#define LED_1_PIN	  GPIO_PIN_1
#define LED_IN_BOARD  LED_1_PIN


#define LED_0		   0
#define LED_0_TOGGLE   HAL_GPIO_TogglePin(GPIOB, LED_0_PIN)
#define LED_0_LOW      HAL_GPIO_WritePin(GPIOB,  LED_0_PIN, GPIO_PIN_SET)
#define LED_0_HIGH     HAL_GPIO_WritePin(GPIOB,  LED_0_PIN, GPIO_PIN_RESET)
#define LED_0_GETSTATE HAL_GPIO_ReadPin(GPIOB,   LED_0_PIN);

#define LED_1		   1
#define LED_1_TOGGLE   HAL_GPIO_TogglePin(GPIOB, LED_1_PIN)
#define LED_1_LOW      HAL_GPIO_WritePin(GPIOB,  LED_1_PIN, GPIO_PIN_SET)
#define LED_1_HIGH     HAL_GPIO_WritePin(GPIOB,  LED_1_PIN, GPIO_PIN_RESET)
#define LED_1_GETSTATE HAL_GPIO_ReadPin(GPIOB,   LED_1_PIN);


#define LED_SEQ_ERROR  -1
#define LED_SEQ_SUCCESS 0
#define LED_SEQ_THREAD	1






/****************************************************************
 * params: sequence: LED_SEQ_THREAD: secuencia para hilos
 * return: nothing
 * descrp: enciende y apaga leds generando una secuencia.
 ****************************************************************/
void led_sequence(int sequence, int led);

#endif
