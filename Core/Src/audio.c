/*******************************************************************************
  * @file           : audio.c
  * @brief          : librerias de audio
  * @author			: Yerih Iturriago
  ******************************************************************************/


#include "audio.h"




/******************************************************************
input	:	header del archivo.
output	:	string de datos
descr	:	retorna el string del error segun frLabel.
*******************************************************************/
void audio_printWaveHeader(WAVE_headerFile_typedef* header)
{
	char a[20];
	utils_32b_hexToString(					header->FileFormat, a);
	printf("     WAVE File Header\n");
	printf("ChunkID       = %lu\n", 	    header->ChunkID);
	printf("FileSize      = %lu bytes\n",   header->FileSize);
	printf("SampleRate    = %lu hz\n", 	    header->SampleRate);
	printf("BitPerSample  = %d\n", 		    header->BitPerSample);
	printf("ByteRate      = %lu byte/s\n",  header->ByteRate);
	printf("FileFormat    = %s\n", 	    	a);
	printf("NbrChannels   = %d\n", 			header->NbrChannels);
	printf("SubChunk1ID   = %lu\n", 		header->SubChunk1ID);
	printf("SubChunk1Size = %lu\n", 		header->SubChunk1Size);
	printf("AudioFormat   = %d\n", 			header->AudioFormat);
	printf("BlockAlign    = %d\n", 			header->BlockAlign);
	printf("SubChunk2ID   = %lu\n", 		header->SubChunk2ID);
	printf("SubChunk2Size = %lu\n", 		header->SubChunk2Size);
//	printf("duration      = %lu seg\n", pad->iFile[i].file.duration);
//	uint32_t duration = header->FileSize / header->ByteRate;
}


/******************************************************************
input	:	header del archivo.
output	:	string de datos
descr	:	retorna el header en string. Recuerde hacer un pvPortFree().
*******************************************************************/
char* audio_waveHeaderToString(WAVE_headerFile_typedef* header)
{
	char str[40];
	char* dst = (char*)pvPortMalloc(sizeof(char)*200);
	memset(str, 	0, sizeof(str));
	memset(dst, 	0, sizeof(sizeof(char)*200));

	strcat(dst, "     WAVE File Header\n");
	sprintf(str, "ChunkID       = %lu\n", 	header->ChunkID);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

	sprintf(str, "FileSize      = %lu bytes\n",   header->FileSize);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

	sprintf(str, "SampleRate    = %lu hz\n", 	    header->SampleRate);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

	sprintf(str, "BitPerSample  = %d\n", 		    header->BitPerSample);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

	sprintf(str, "ByteRate      = %lu byte/s\n",  header->ByteRate);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

	sprintf(str, "FileFormat    = 0x%lx\n", 	    header->FileFormat);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

	sprintf(str, "NbrChannels   = %d\n", 			header->NbrChannels);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

	sprintf(str, "SubChunk1ID   = %lu\n", 		header->SubChunk1ID);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

	sprintf(str, "SubChunk1Size = %lu\n", 		header->SubChunk1Size);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

	sprintf(str, "AudioFormat   = %d\n", 			header->AudioFormat);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

	sprintf(str, "BlockAlign    = %d\n", 			header->BlockAlign);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

	sprintf(str, "SubChunk2ID   = %lu\n", 		header->SubChunk2ID);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

	sprintf(str, "SubChunk2Size = %lu\n", 		header->SubChunk2Size);
	strcat(dst, str);
	memset(str, 	0, sizeof(str));

//	printf("duration      = %lu seg\n", pad->iFile[i].file.duration);
//	uint32_t duration = header->FileSize / header->ByteRate;
	return dst;
}

void audio_readWavFileUntilEnd(FIL* SDFile, void* dst, uint32_t len)
{
	uint32_t lseek = f_lseek(SDFile, 44);
	printf("reading file. lseek = %lu...\n", lseek);
	SD_readFile(SDFile, dst, len, &lseek);
	printf("lseek = %lu\n", lseek);
}

void audio_cleanAudioBuffer_16b(int16_t* b, uint32_t len)
{
	for(uint32_t i = 0; i < len; i++)
		b[i] = 0;
}
