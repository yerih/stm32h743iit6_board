/*******************************************************************************
  * @file           : utils.h
  * @brief          : utilidades
  * @author			: Yerih Iturriago
  * @note			:
  *
  ******************************************************************************/

#include "utils.h"



/******************************************************************
function: 	utils_dresult
input	:	dLabel: referido al enum DSTATUS.
output	:	string de datos
descr	:	retorna el string del error segun frLabel.
*******************************************************************/
char* utils_dresult(uint8_t dLabel, char* str)
{
	switch(dLabel)
	{
		case RES_OK:		sprintf(str, "Successful"); 	   break;
		case RES_ERROR:		sprintf(str, "R/W Error");		   break;
		case RES_WRPRT:		sprintf(str, "Write Protected");   break;
		case RES_NOTRDY:	sprintf(str, "Not Ready");		   break;
		case RES_PARERR:	sprintf(str, "Invalid Parameter"); break;
		default: 			sprintf(str, "INVALID LABEL, check dLabel param"); break;
	}
	return str;
}

/******************************************************************
function: 	utils_fresult
input	:	frLabel: referido al enum FRESULT.
output	:	string de datos
descr	:	retorna el string del error segun frLabel.
*******************************************************************/
char* utils_fresult(uint8_t frLabel, char* str)
{
	unsigned int len = sizeof(str);
	memset(str, 0, len);
	switch(frLabel)
	{
		case FR_OK:					 sprintf(str, "%s", "Success");										break;
		case FR_DISK_ERR:			 sprintf(str, "%s", "Error in the low level disk I/O layer");		break;
		case FR_INT_ERR:			 sprintf(str, "%s", "Assertion failed");							break;
		case FR_NOT_READY:			 sprintf(str, "%s", "The physical drive cannot work");				break;
		case FR_NO_FILE:			 sprintf(str, "%s", "Could not find the file");						break;
		case FR_NO_PATH:			 sprintf(str, "%s", "Could not find the path");						break;
		case FR_INVALID_NAME:		 sprintf(str, "%s", "The path name format is invalid");				break;
		case FR_DENIED:				 sprintf(str, "%s", "Access denied or directory full");				break;
		case FR_EXIST:				 sprintf(str, "%s", "Access denied due to prohibited access");		break;
		case FR_INVALID_OBJECT:		 sprintf(str, "%s", "The file/directory object is invalid");		break;
		case FR_WRITE_PROTECTED:	 sprintf(str, "%s", "The physical drive is write protected");		break;
		case FR_INVALID_DRIVE:		 sprintf(str, "%s", "The logical drive number is invalid");			break;
		case FR_NOT_ENABLED:		 sprintf(str, "%s", "The volume has no work area");					break;
		case FR_NO_FILESYSTEM:		 sprintf(str, "%s", "There is no valid FAT volume");				break;
		case FR_MKFS_ABORTED:		 sprintf(str, "%s", "The f_mkfs() aborted due to any problem");		break;
		case FR_TIMEOUT:			 sprintf(str, "%s", "Couldn't access the volume by Timeout");		break;
		case FR_LOCKED:				 sprintf(str, "%s", "Rejected according to the sharing policy");	break;
		case FR_NOT_ENOUGH_CORE:	 sprintf(str, "%s", "LFN working buffer could not be allocated");	break;
		case FR_TOO_MANY_OPEN_FILES: sprintf(str, "%s", "Number of open files > _FS_LOCK");				break;
		case FR_INVALID_PARAMETER:	 sprintf(str, "%s", "Given parameter is invalid");					break;
		default:					 sprintf(str, "%s", "FRsult doesn't exist");						break;
	}
	return str;
}

/******************************************************************
input	:	hLabel: referido al enum HAL STATUS.
output	:	string de datos
descr	:	retorna el string del error segun frLabel.
*******************************************************************/
char* utils_hresult(uint8_t hLabel, char* str)
{
	unsigned int len = sizeof(str);
	memset(str, 0, len);
	switch(hLabel)
	{
		case HAL_OK:		sprintf(str, "Successful"); 	   break;
		case HAL_ERROR:		sprintf(str, "HAL Error");		   break;
		case HAL_BUSY:		sprintf(str, "HALL busy");   	   break;
		case HAL_TIMEOUT:	sprintf(str, "HALL timeout");	   break;
		default: 			sprintf(str, "INVALID LABEL, check Label param = %d", hLabel); break;
	}
	return str;
}


/**********************************************************************************
input	:	v: value of 4 bytes. dst: string destiny.
output	:	n/a
descr	:	convierte un numero hex, invertido y contenido en un entero, en string
**********************************************************************************/
void utils_32b_hexToString(uint32_t v, char dst[])
{
	//45564157
	char str[8];
	char r[5];
	char *p[20];
	long temp;
	int j = 0;
	for (int i = 4; i > 0; i--)
	{
	  sprintf (str, "%lx", v);
	  temp = (strtol(str+(i-1)*2, p, 10))/(int)pow(10, j*2);
	  r[j++] = (char)((temp - (temp / 10) * 10)+(temp/10)*16);
	}
    sprintf(dst, "%s", r);
}

