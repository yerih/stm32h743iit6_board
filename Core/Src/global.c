/*******************************************************************************
  * @file           : global.c
  * @brief          : incluye todas las librerias y variables globales
  * @author			: Yerih Iturriago
  ******************************************************************************/

#include "global.h"



int g_showDebug = FALSE;
int g_SDmntState = FALSE;//indica si una sd esta montada con sd_mount()
//extern st_PAD pad_snare;
//
//#if !defined(ONLY_SNARE) || defined(WITH_KICK)
//extern st_PAD pad_kick;
//#endif
//
//#if !defined(ONLY_SNARE) || defined(WITH_HITHAT)
//extern st_PAD pad_hithat;
//#endif
//
//#ifndef ONLY_SNARE
//extern st_PAD pad_tom1;
//extern st_PAD pad_tom2;
//extern st_PAD pad_tom3;
//extern st_PAD pad_cymbal_L;
//extern st_PAD pad_cymbal_R;
//#endif
//
//
//extern DSTATUS SD_initialize(BYTE lun);
