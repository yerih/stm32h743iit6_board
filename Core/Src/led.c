/*******************************************************************************
  * @file           : led.c
  * @brief          : incluye todas las librerias y variables globales
  * @author			: Yerih Iturriago
  ******************************************************************************/

#include "led.h"


/****************************************************************
 * params: sequence: LED_SEQ_THREAD: secuencia para hilos
 * return: nothing
 * descrp: enciende y apaga leds generando una secuencia.
 ****************************************************************/
void led_sequence(int sequence, int led)
{
	int i = 0;
	uint32_t delay;
	if(led != LED_0 && led != LED_1 && led != LED_IN_BOARD)
		return;

	switch(sequence)
	{
		case LED_SEQ_ERROR:   delay = 50;  break;
		case LED_SEQ_SUCCESS: delay = 500; break;
		case LED_SEQ_THREAD:  delay = 100; break;
		default: delay = 300; break;
	}

	switch(sequence)
	{
		case LED_SEQ_THREAD:  	while(1)
								{
									for(int i = 0;i < 3; i++)
									{
										led == LED_0 ? LED_0_TOGGLE:LED_1_TOGGLE;
										HAL_Delay(delay);
									}
									for(int i = 0;i < 3; i++)
									{
										led == LED_0 ? LED_0_TOGGLE:LED_1_TOGGLE;
										HAL_Delay(1000);
									}
								}
								break;

		case LED_SEQ_ERROR:		for(i = 0; i < 100; i++)
								{
									led == LED_0 ? LED_0_TOGGLE:LED_1_TOGGLE;
//									osDelay(delay);
									HAL_Delay(delay);
								}
								break;
		default:
								for(i = 0; i < 100; i++)
								{
									led == LED_0 ? LED_0_TOGGLE:LED_1_TOGGLE;
									HAL_Delay(delay);
								}
								break;
	}
}
