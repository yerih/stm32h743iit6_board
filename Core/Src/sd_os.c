/*******************************************************************************
  * @file           : sd_os.c
  * @brief          : manipulacion de archivo usando SD
  * @author			: Yerih Iturriago
  ******************************************************************************/

#include "sd_os.h"





/******************************************************************
function: 	SD_mount
input	:	show: TRUE: muestra msj en pantalla
output	:	TRUE: si hubo algun error.
descr	:	realiza montaje de SD. Se debe llamar a SD_unmount().
			despues de realizar todas operaciones con la SD.
*******************************************************************/
uint8_t SD_mount(void)
{
	uint8_t ret;
	ret = f_mount(&SDFatFS, SDPath, 1);
	if(g_showDebug)
		printf("f_mount. ret = %d\n", ret);
	if(ret == FR_OK)
	{
		if(g_showDebug)
			printf("SD: Acceso concedido\n");//lcd_printf("SD: Acceso concedido\n");
		g_SDmntState = 1;
		return 0;
	}
	if(g_showDebug)
			printf("SD: error de montaje\n");//lcd_printf("SD: error de montaje\n");
	g_SDmntState = 0;
	return 1;
}

/******************************************************************
function: 	SD_mount
input	:	show: TRUE: muestra msj en pantalla
output	:	TRUE: si hubo algun error.
descr	:	realiza montaje de SD. Se debe llamar a SD_unmount().
			despues de realizar todas operaciones con la SD.
*******************************************************************/
uint8_t SD_unmount(void)
{
	if(f_mount(NULL, SDPath, 1) == FR_OK)
	{
		if(g_showDebug)
			printf("SD: desmontada\n");
		g_SDmntState = 0;
		return 0;
	}
	if(g_showDebug)
		printf("SD: error de desmonte\n");
	return 1;
}

/******************************************************************
input	:	nothing
output	:	TRUE: si hubo algun error.
descr	:	realiza montaje de SD. Se debe llamar a SD_unmount().
			despues de realizar todas operaciones con la SD.
*******************************************************************/
uint8_t SD_init(void)
{
	if(SD_mount())
	{
		g_showDebug ? printf("SD_init: sd is not mounted\n"):0;
		return 1;
	}
//#ifdef INC_FREERTOS_H
//	osDelay(500);
//#endif
	return 0;
}

/******************************************************************
function: 	SD_openFile
input	:	mode: WRITE: crea el archivo y lo edita.
				  READ : lee el archivo.

output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Abre un archivo de la SD segun el modo. Se debe
			realizar montaje de SD con SD_mount. Se debe cerrar el
			archivo luego de usarlo.
*******************************************************************/
uint8_t SD_openFile(char* fileName, uint8_t mode)
{
	uint8_t ret;
	char str[50];
	memset(str, 0, sizeof(str));
	if(g_SDmntState == 0){
		printf("sd_openFile: check sd mount and sd card in slot\n");
	}


	if( (ret = f_open(&SDFile, fileName, mode)) != FR_OK)
	{
		if(g_showDebug)
			printf("sd_openFile: %s . ret = %s, %s, line %d\n", fileName, utils_fresult(ret, str), __FILE__, __LINE__);
		return ret;
	}
	return 0;
}


/******************************************************************
function: 	SD_closeFile
input	:	SDFile: apuntador hacia el archivo SD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Cierra el archivo FIL de la SD.
*******************************************************************/
uint8_t SD_closeFile(FIL *SDFile)
{
	uint8_t ret = 0;
	if( (ret = f_close(SDFile)) != FR_OK)
		return ret;
	return ret;
}

/***********************************************************************
function: 	SD_writeFile
input	:	fileName: nombre del archivo con su extension.
			buffer  : direccion donde se almacenará la data leida.
			len		: tmno del buffer o de la data.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Abre un archivo de la SD en modo FA_READ o FA_OPEN_EXISTING.
************************************************************************/
uint8_t SD_writeFile(char* fileName, void* buffer, uint32_t len)
{
	uint32_t bytesRead = 0;

	if(f_write(&SDFile, buffer, (UINT)len, (UINT*)&bytesRead) == FR_OK)
	{
		if(g_showDebug)
			printf("SD: se escribieron %lu bytes\n", bytesRead);
	}
	else
	{
		if(g_showDebug)
			printf("SD: Error. Se escribieron %lu bytes\n", bytesRead);
	}

	return 0;
}



/***********************************************************************
function: 	SD_readFile
input	:	fileName: nombre del archivo con su extension.
			buffer  : direccion donde se almacenará la data leida.
			len		: tmno del buffer o de la data.
			*lseek  : apuntador hacia el cursor u offset de R/W del
					  archivo.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Abre un archivo de la SD en modo FA_READ o FA_OPEN_EXISTING
			y lo cierra.
************************************************************************/
uint8_t SD_readFile(FIL* SDFile, void* buffer, uint32_t len, uint32_t* lseek)
{
	uint32_t a;
	if(len == 0)return 1;
	f_lseek(SDFile, *lseek);
	f_read(SDFile, buffer, len, (UINT*)&a);
	g_showDebug ? printf("SD: lseek = %lu, len = %lu\n", *lseek, len):0;
	(*lseek) += a;
	g_showDebug ? printf("SD: Se leyeron: %lu bytes\n", a):0;
	return 0;
}


/***********************************************************************
function: 	SD_lseek
input	:	fileName    : nombre del archivo con su extension.
			lseek       : cursor u offset de R/W del archivo.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	establece el lseek o cursor de R/W del archivo.
************************************************************************/
uint8_t SD_lseek(char* fileName, uint32_t lseek)
{
	int ret;
	if(g_showDebug)
		printf("SD_lseek: entry\n");
	ret = f_lseek(&SDFile, lseek);
	if(g_showDebug)
		printf("SD_lseek: ret = %d\n", ret);
	return ret == FR_OK;
}








